package com.binary_studio.fleet_commander.core.ship.util;

import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public class SystemPG {

    private DefenciveSubsystem defenciveSubsystem;
    private AttackSubsystem attackSubsystem;

    public SystemPG() {
        defenciveSubsystem = null;
        attackSubsystem = null;
    }

    public DefenciveSubsystem getDefenciveSubsystem() {
        return defenciveSubsystem;
    }

    public void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
        this.defenciveSubsystem = defenciveSubsystem;
    }

    public AttackSubsystem getAttackSubsystem() {
        return attackSubsystem;
    }

    public void setAttackSubsystem(AttackSubsystem attackSubsystem) {
        this.attackSubsystem = attackSubsystem;
    }

    public Integer generalSystemPG() {
        Integer generalPG = 0;
        if (attackSubsystem != null) generalPG += attackSubsystem.getPowerGridConsumption().value();
        if (defenciveSubsystem != null) generalPG += defenciveSubsystem.getPowerGridConsumption().value();
        return generalPG;
    }
}
