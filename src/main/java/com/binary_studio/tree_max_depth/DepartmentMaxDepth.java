package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

public final class DepartmentMaxDepth {
	public static void main(String[] args) {
//		Department root1 = new Department("A",new Department("B",new Department("C",
//				new Department("G"),new Department("G1",new Department("GA")))),new Department("B1"));
//
//		Department root1 = new Department("A",new Department("B", new Department("C")));
//		Department root2 = new Department("A",
//				new Department("null" , new Department("C" , new Department("null"),null,null)),
//		new Department("B1", new Department("null" , new Department("C1", new Department("D1")))));
//		System.out.println(calculateMaxDepth(root2).toString());
	}

	private DepartmentMaxDepth() {

	}
	public static Integer calculateMaxDepth(Department rootDepartment) {
		ArrayList<Integer> depths = new ArrayList<>();
		if(rootDepartment == null) return 0;
		else
		{
			int max = 0;
			for(int i = 0;i< rootDepartment.subDepartments.size();i++)
			{
				depths.add(-1);
				depths.set(i,calculateMaxDepth(rootDepartment.subDepartments.get(i)));
                if(max <  depths.get(i))max = depths.get(i);
			}
			if(rootDepartment == null) return max;
			else return max+1;
		}
	}
}
