package com.binary_studio.uniq_in_sorted_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static void main(String[] args) {
//		Stream<Row<Integer>> stream = Stream.of(
//				new Row<Integer>((long) 1),
//				new Row<Integer>((long) 2),
//				new Row<Integer>((long) 2),
//				new Row<Integer>((long) 4),
//				new Row<Integer>((long) 5)
//		);
//		System.out.println(uniqueRowsSortedByPK(stream).collect(Collectors.toList()));
	}
	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.distinct();
	}

}
